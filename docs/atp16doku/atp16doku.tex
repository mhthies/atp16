\documentclass[a4paper, parskip=half, DIV14]{scrartcl}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}

% Layout
\usepackage{libertine}
\usepackage{courier}
\usepackage{listings}
\lstset{
    basicstyle=\footnotesize\ttfamily,
    numbers=left,
    numberstyle=\footnotesize,
    stepnumber=1,
    numbersep=5pt,
    %backgroundcolor=\color{white},
    showspaces=false,
    showtabs=false,
    tabsize=2,
    %frame=single,
    %captionpos=b, % set caption position to bottom
    breaklines=true,
    breakatwhitespace=false, % break at whitespaces only?
    keywordstyle=\bfseries, % key words are highlighted with bold font
    showstringspaces=false,
    framexleftmargin=5mm,
    texcl=true, % enables Tex-style comment lines
    escapechar=|| % text within defined char is escaped to \LaTeX, eg. \|| \LaTex \||
}
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage{microtype}
\usepackage[unicode]{hyperref}

% Metadata
\title{LED Feedback Controller}
\subtitle{ATP-Projekt -- Dokumentation}
\author{Daniel Buschmann, Marcel Schneider, Michael Thies, Lars Wollert}
\date{März 2018}

\begin{document}
    \maketitle

    \section{System-Architektur}
        Das LED-Feedback-System besteht aus zwei Software-Komponenten: Der \lstinline{led_feedback_controller} enthält
        die REST- und MQTT-Schnittstellen, die Zustandsverwaltung und Animationserzeugung. Zur Darstellung der Farben
        auf den LED-Streifen schickt er die erzeugten Farbwerte zyklisch an den \lstinline{led_daemon}. Dieser verwendet
        die \lstinline{rpi_ws281x}-Library, um mittels DMA und Hardware-PWM-Erzeugung die WS2811-LEDs anzusteuern. Für
        den direkten Zugriff auf den Arbeitsspeicher benötigt er root-Rechte.
        
        Der \lstinline{led_feedback_controller} ist modular aufgebaut: \lstinline{frontend_rest} und
        \lstinline{frontend_mqtt} starten -- je nach Programmparametern -- die jeweiligen Schnittstellen, behandeln
        darüber empfangene Nachrichten und geben die resultierenden Zustands-Updates an den \lstinline{state_controller}
        weiter. Dieser verwaltet alle erfassten Zustände und berechnet daraus eine Darstellung, bestehend aus
        Animationstyp (\lstinline{pattern}), Farbe, Geschwindigkeit, usw. Die bestimmte Darstellung wird an den
        \lstinline{pattern_generator} weitergegeben, der aus dieser Information und der aktuellen Systemzeit zyklisch
        Farbwerte für die LEDs berechnet und an ein \lstinline{backend} weitergibt.
        
        Das \lstinline{backend_led} sendet die Farbwerte dann als kombinierte, ASCII-formatierte Ganzzahlen über eine
        Named-Pipe an den \lstinline{led_daemon}. Zu Debugging-Zwecken steht auch ein GUI-Backend basierend auf
        PyQt5 zur Verfügung (\lstinline{backend_qt}), dass die Farben auf dem Bildschirm, statt auf den LEDs
        anzeigt.
        
        Weiterhin steht im Python-Modul \lstinline{led_test_gui} eine PyQt5-basierte GUI zur Verfügung, die
        sich per MQTT- oder REST-Schnittstelle mit dem LED-Feedback-Controller verbinden kann, um ihm beispielhafte
        Zuständen zu senden.
        
        \subsection{Software-Abhängigkeiten}
            Alle Programmteile sind in Python 3 programmiert. Zur Ausführung wird somit der Python 3-Interpreter
            benötigt (getestet mit Version 3.5.3 und 3.6.4).
            
            Für das MQTT-Interface wird das Python-Modul \lstinline{paho-mqtt} benötigt, für den Start des LED-Daemons
            die \lstinline{rpi_ws281x}-Library inklusive Python 3-Wrapper, für die Debug-GUI \lstinline{pyqt5}.
            
            Das \lstinline{led_test_gui}-Programm benötigt ebenfalls \lstinline{pyqt5} und \lstinline{paho-mqtt}.
            
            Zum einfachen Handling der benötigten Python-Module, empfiehlt sich die Verwendung einer virtuellen
            Python-Umgebung mit \lstinline{virtualenv}. Auf dem Raspberry Pi kann eine solche angelegt, aktiviert und
            eingerichtet werden mit
            \begin{lstlisting}[numbers=none]
$ sudo apt install python3-venv
$ python3 -m venv venv
$ . venv/bin/activate
$ pip3 install paho-mqtt rpi_ws281x
            \end{lstlisting}
            
        \subsection{Start der Komponenten}
            Der Python-Interpreter sollte im \lstinline{src/}-Verzeichnis der Software gestartet werden. Andernfalls ist
            sicherzustellen, dass das \lstinline{src/}-Verzeichnis teil des \lstinline{$PYTHON_PATH}s ist. Für den
            LED-Daemon lautet der Befehl dann
            \begin{lstlisting}[numbers=none]
$ cd atp16/src/
$ sudo python3 -m led_daemon.main
            \end{lstlisting}
            
            Der LED-Feedback-Controller wird gestartet mit
            \begin{lstlisting}[numbers=none]
$ sudo python3 -m led_feedback_controller.main <parameter>
            \end{lstlisting}
            
            Als Parameter können übergeben werden: \lstinline{--rest [PORT]} (startet das REST-Interface auf dem
            angegebenen Port), \lstinline{--mqtt [HOST]} (startet den MQTT-Client mit Verbindung zum Broker auf dem
            angegebenen Host), \lstinline{--gui} (startet die Debug-GUI, statt die Farbwerte an den LED-Daemon zu
            schicken).
            
            Um die Named-Pipe, über die LED-Feedback-Controller und LED-Daemon miteinander kommunizieren an einere
            anderen Stelle im Datei-System anzulegen (Standard: \lstinline{/tmp/communication.fifo}), kann der
            alternative Pfad als Argument an den Aufruf des LED-Daemons angehängt werden und mit dem Parameter
            \lstinline{--led-pipe [PFAD]} dem LED-Feedback-Controller übergeben werden.
            
            Eine vollständige Liste der Parameter kann mit \lstinline{--help} abgerufen werden.
            
            \paragraph{Automatischer Start}
            Um die Komponenten automatisch zu starten, werden auf dem Raspberry Pi zwei Systemd-Units angelegt:
            \lstinline{/etc/systemd/system/led-daemon.service} und
            \lstinline{led-feedback.service}.
            Diese sorgen für den automatischen Start beim Booten und einen Neustart nach 3s im Falle eines Absturzes und
            beachten dabei die einseitige Abhängigkeit des Feedback-Controllers vom LED-Daemon. Der Start des
            Feedback-Controllers mit reduzierten Rechten (als User \lstinline{pi}) und die Start-Parameter der Programme
            werden ebenfalls hier definiert.

            Somit können die üblichen Systemd-Befehle verwendet werden, um den Log zu lesen oder die Komponenten
            euzustarten:
            \begin{lstlisting}[numbers=none]
$ sudo journalctl -u led-feedback
$ sudo systemctl restart led-feedback
            \end{lstlisting}

    \newpage
    \section{Interface-Spezifikation}
        Der LED-Feedback-Controller auf dem Raspberry Pi erhält die Informationen über die aktuellen Systemzustände
        wahlweise über eine REST-Schnittstelle oder das MQTT-Protokoll. Dazu wurde sowohl ein HTTP-Server, als auch ein
        MQTT-Client integriert, die auch parallel genutzt werden können.
        
        Um den HTTP/REST-Server zu nutzen ist die beim Start des LED Feedback Controllers die entsprechende Option,
        sowie ein TCP-Port anzugeben:
        \begin{lstlisting}[numbers=none]
$ python3 -m led_feedback_controller.main --rest 8000
        \end{lstlisting}
        Auf dem angegebenen Port nimmt der LED Feedback Controller anschließend HTTP-Requests entgegen, die gemäß dem
        in Abschnitt \ref{REST} angegebenen Protokoll zum Aktualisieren des Systemzustands für das optische Feedback
        genzutzt werden können.
        
        Zum Starten des MQTT-Clients ist ebenfalls eine Option anzugeben, die als ergänzenden Parameter die Adresse des
        MQTT-Brokers erhält. Dies kann eine IP-Adresse oder ein Hostname sein:
        \begin{lstlisting}[numbers=none]
$ python3 -m led_feedback_controller.main --mqtt 10.0.0.42
        \end{lstlisting}
        Der MQTT-Client wird versuchen, sich mit einer unverschlüsselten TCP-Verbindung zum Standardport 1883 des
        angegebenen Servers zu verbinden. Sollte der MQTT-Broker nicht erreichbar sein oder reagieren, wird der
        Verbindungsaufbau regelmäßig wiederholt. Gleiches passiert bei einem Abbruch der Verbindung zum Broker. Somit
        ist die Verbindung robust gegen unterschiedliche Start-Reihenfolgen und Netzwerkunterbrechungen.
        
        \subsection{REST-Interface}
            \label{REST}
            \paragraph{Requests}
            Die Anfragen an das REST-Interface sind als HTTP-POST-Request zu übermitteln. Für die unterschiedlichen
            Zustände und Trigger stehen verschiedene virtuelle Endpunkte (Pfade) zur Verfügung (vgl. Abschnitt
            \ref{section:RESTendpoints}). Der zu setzende Zustand
            und seine Parameter müssen als einzelne Formularfelder übermittelt werden und können im Request als
            \lstinline{multipart/form-data} oder \lstinline{application/x-www-form-urlencoded} kodiert werden, wobei der
            \lstinline{Content-Type}-Request-Header entsprechend gesetzt sein muss.
            
            \paragraph{Response}
            Die HTTP-Response kann ausgewertet werden, um den Erfolg des Requests zu beurteilen. Darüber gibt sowohl der
            HTTP-Statuscode, als auch der JSON-kodierte Inhalt der Antwort Aufschluss.
            
            Bei einer erfolgreich bearbeiteten Anfrage wird die Antwort mit Status 200 (OK) gesendet und das JSON-Objekt
            darin enthält ein Feld \lstinline{"success"} mit dem Wert \lstinline{true}.
            
            Bei einer Anfrage an einen unbekannten Endpunkt oder mit der falschem HTTP-Methode wird die Antwort
            entsprechend mit dem Statuscode HTTP 400 (Not Found) oder HTTP 405 (Method not Allowed) quittiert. Das 
            zurückgegebene JSON-Objekt enthält ein Feld \lstinline{"error"} mit der ausführlichen Fehlerbeschreibung als
            String.
            
            Entsprechen die übermittelten Daten nicht dem erwarteten Format, lautet der HTTP Statuscode 422 
            (Unprocessable Entity); bei einem internen Programmfehler bei der Verarbeitung HTTP 500 (Internal Server
            Error). In beiden Fällen wird ebenfalls das \lstinline{"error"}-Feld mit einer textuellen Beschreibung
            zurückgegeben.
            
            \paragraph{Einfache Tests}
            Zum Testen des REST-Interfaces kann entweder die LED-Test-GUI verwendet werden oder \lstinline{curl} auf der
            (Linux-)Kommandozeile:
            \begin{lstlisting}[numbers=none]
$ curl -d <paramter>=<wert> <host>:<port>/<endpoint>
            \end{lstlisting}

            Beispiele, wenn der LED-Feedback-Controller auf dem eigenen Rechner mit REST-Schnittstelle auf Port 8000
            läuft:
            \begin{lstlisting}[numbers=none]
$ curl -d state=3 localhost:8000/machine/state
$ curl -d state=3 -d progress=0.6 localhost:8000/machine/state
$ curl -d value=0.45 localhost:8000/terminal/bargraph
$ curl -d - localhost:8000/terminal/feedback
            \end{lstlisting}
            
            \subsubsection{Endpunkte}
                \label{section:RESTendpoints}
                Die Liste der möglichen REST-Endpunkte/-Pfade kann Tabelle \ref{table:RESTendpoints} entnommen werden.
                Die Maschinenzustände und Terminalzustände müssen jeweils durch ihre ID gemäß Tabellen
                \ref{table:machinestate} \& \ref{table:terminalstate} kodiert werden. Die \lstinline{progress}-,
                \lstinline{error_position}- und \lstinline{value}-Parameter werden als Dezimalzahl im Wertebereich
                $[0,1]$ erwartet.
                
                Der \lstinline{progress} wird nur von den Maschinenstatus SLOW und RUNNING und den Terminalstatus
                BUSY\_BACKGROUND und BUSY ausgewertet. Die \lstinline{error_position} wird nur von den Maschinenstatus
                WARNING, ERROR, CRITICAL und EXPLODING ausgewertet.
                
                Der \lstinline{/ping}-Endpunkt dient dazu, die Erreichbarkeit des REST-Servers zu testen. Ein
                GET-Request an diesen Endpunkt sollte stets mit HTTP 200 (OK) beantwortet werden. Zusätzlich enthält die
                Antwort ein JSON-Objekt mit dem Feld \lstinline{ts}, das den aktuellen Zeitstempel des Servers im
                ISO-Format enthält.
                
                \begin{table}[h]
                    \begin{tabularx}{\textwidth}{llX}
                        \toprule
                        Zustand          & Pfad                           & Parameter\\
                        \midrule
                        Maschinenzustand & \lstinline{/machine/state}     & \lstinline{state}, \lstinline{progress} (optional), \lstinline{error_position} (optional)\\
                        Terminalzustand  & \lstinline{/terminal/state}    & \lstinline{state}, \lstinline{progress} \\
                        Input-Feedback   & \lstinline{/terminal/feedback} & \\
                        Bargraph         & \lstinline{/terminal/bargraph} & \lstinline{value} \\
                        \textit{Ping}    & \lstinline{/ping}              & \\
                        \bottomrule
                    \end{tabularx}
                    \caption{REST-Endpunkte}
                    \label{table:RESTendpoints}
                \end{table}
            

        \subsection{MQTT-Interface}
            \label{MQTT}
            Um über die MQTT-Schnittstelle die vom LED-Feedback-Controller dargestellten Zustände zu aktualisieren, muss
            nur eine Nachricht an das jeweilige MQTT-Topic (vgl. Abschnitt \ref{section:MQTTtopics}) auf dem verbundenen
            MQTT-Broker gesendet werden. Die Nachrichten müssen einen ASCII-kodierten String enthalten, der die
            einzelnen Parameter des Zustands Leerzeichen-getrennt als Zahlen enthält.
            
            Alle relevanten MQTT-Topics liegen unterhalb von \lstinline{feedback/led}. Dieses Basis-Topic muss bei
            Bedarf im Quellcode (\lstinline{src/led_feedback_controller/frontend_mqtt.py}) geändert werden.
            
            \paragraph{Einfache Tests}
            Zum Testen des MQTT-Interfaces kann z.B. der einfache MQTT-Broker \lstinline{mosquitto} und die LED-Test-GUI
            oder der Kommandozeilen-Client \lstinline{mosquitto_pub} verwendet werden.
            
            \begin{lstlisting}[numbers=none]
$ mosquitto_pub -h <host> -t <topic> -m <nachricht>
            \end{lstlisting}

            Beispiele, wenn der MQTT-Broker auf dem eigenen Rechner läuft:
            \begin{lstlisting}[numbers=none]
$ mosquitto_pub -t feedback/led/machine/state -m 3
$ mosquitto_pub -t feedback/led/machine/state -m "3 0.6"
$ mosquitto_pub -t feedback/led/terminal/bargraph -m 0.45
$ mosquitto_pub -t feedback/led/terminal/feedback -n
            \end{lstlisting}
            
            \subsubsection{Topics}
                \label{section:MQTTtopics}
                Die Liste der möglichen MQTT-Topics kann Tabelle \ref{table:MQTTtopics} entnommen werden.
                Die Maschinenzustände und Terminalzustände müssen jeweils durch ihre ID gemäß Tabellen
                \ref{table:machinestate} \& \ref{table:terminalstate} kodiert werden. Die \lstinline{progress}-,
                \lstinline{error_position}- und \lstinline{value}-Parameter werden als Dezimalzahl im Wertebereich
                $[0,1]$ erwartet.
                
                Der \lstinline{progress} wird nur von den Maschinenstatus SLOW und RUNNING und den Terminalstatus
                BUSY\_BACKGROUND und BUSY ausgewertet. Die \lstinline{error_position} wird nur von den Maschinenstatus
                WARNING, ERROR, CRITICAL und EXPLODING ausgewertet.
                
                Das \lstinline{ping}-Topic dient dazu, die Verbindung des LED-Feedback-Controllers zu testen. Eine
                (ggf. leere) Nachricht an dieses Topic sollte stets mit einer Nachricht and das Topic
                \lstinline{feedback/led/pong} beantwortet werden, die den aktuellen Zeitstempel des
                LED-Feedback-Controllers im ISO-Format enthält.
                
                \begin{table}[h]
                    \begin{tabularx}{\textwidth}{llX}
                        \toprule
                        Zustand          & Topic                                      & Nachricht\\
                        \midrule
                        Maschinenzustand & \lstinline{feedback/led/machine/state}     & <state> [<progress> [<error\_position>]]\\
                        Terminalzustand  & \lstinline{feedback/led/terminal/state}    & <state> [<progress>]\\
                        Input-Feedback   & \lstinline{feedback/led/terminal/feedback} & \\
                        Bargraph         & \lstinline{feedback/led/terminal/bargraph} & <value> \\
                        \textit{Ping}    & \lstinline{feedback/led/ping}              & \\
                        \bottomrule
                    \end{tabularx}
                    \caption{MQTT-Topics}
                    \label{table:MQTTtopics}
                \end{table}
    
    
    
    \newpage
    \section{Zustände und Darstellung}
    
    \begin{table}[h]
        \begin{tabularx}{\textwidth}{llX}
            \toprule
            ID &    Name      &    Darstellung\\
            \midrule
            0  &    OFF       &    Schwarz\\
            1  &    READY     &    Leichter schwach grüner Puls\\
            2  &    SLOW      &    Langsame grüne Wellenanimation mit Vorschrittsanzeige\\
            3  &    RUNNING   &    Grüne Wellenanimation mit Vorschrittsanzeige\\
            4  &    WARNING   &    Gelbe Wellenanimation ggf. mit Fehlerpositions-Anzeige in Weiß\\
            5  &    ERROR     &    Rot blinkend, ggf. mit Fehlerpositions-Anzeige in Weiß\\
            6  &    CRITICAL  &    Schnell rot blinkend, ggf. mit Fehlerpositions-Anzeige in Weiß\\
            7  &    EXPLODING &    Von außen nach innen zusammenlaufender roter Streifen, ggf. mit
                                   Fehlerpositions-Anzeige in Weiß\\
            \bottomrule
        \end{tabularx}
        \caption{Maschinenzustände}
        \label{table:machinestate}
    \end{table}

    \begin{table}[h]
        \begin{tabularx}{\textwidth}{llX}
            \toprule
            ID &    Name      &    Darstellung\\
            \midrule
            0  & OFF                   & Schwarz\\
            1  & READY                 & Langsamer blauer Puls\\
            2  & IN\_USE               & Grünblaue Wellenanimation\\
            3  & BUSY\_BACKGROUND      & Grünblauer Hintergrund mit einem gelbem Slider oder gelber wellenförmiger
                                         Fortschrittsanzeige\\
            4  & BUSY                  & Grünblauer Hintergrund mit einem schnellem gelbem Slider oder einer gelben
                                         wellenförmigen Fortschrittsanzeige\\
            5  & REQUESTING\_ATTENTION & Doppelt blinkender hellblauer Puls\\
            \bottomrule
        \end{tabularx}
        \caption{Terminalzustände}
        \label{table:terminalstate}
    \end{table}

    \paragraph{Feedback}
    Der Feedback-Input wird benutzt, um Input von Benutzern am Terminal zu signalisieren. Dabei wird ein weißer Puls
    für kurze Zeit eingeblendet, welcher nach kurzer Zeit verblasst. Diese Art des Inputs hat die höchste Prioritat
    aller Overlays und überschreibt alle anderen Anzeigesignale auf dem oberen LED-Streifen.

    \paragraph{Bargraph}
    Der Bargraph ist dazu gedacht, Eingabewerte darzustellen. Dazu wird kurz ein auf den maximalen Endwert von 1,0
    normierter Fortschrittsbalken in weiß eingeblendet.
    Der Bargraph blendet die Overlays anderer Terminalzustände aus und verringert die Helligkeit der Hintergrundfarben.
    Dadurch kann ein Benutzer eine Änderung einstellen und visuell wahrnehmen und gleichzeitig den Terminalzustand im
    Blick haben. Die Anzeige wird nach 1,5 Sekunden ausgeblendet, wenn keine weitere Änderung vorgenommen wird.

\end{document}
