"""
The led_daemon is used to control the LED strips. The program received the data via a named pipe and sends it to the
strips. The named pipe is created after the initialization of the LED strips. In the named pipe a string is expected
which contains all color values as 24 bit integer (R,G,B). The first line contains the color values of the upper LED
strip one color value of a LED after the other and the second line contains the color values of the lower strip.
Every time the named pipe is read out, the data length is checked and, if the length is incorrect, read out again.
The program is terminated with "CTRl + C". After this, the named pipe is closed, deleted and der LEDs are switched off.
"""

import sys, os

from led_daemon import led_wrapper

# LED strip configuration: UPPER STRIP
LED_1_COUNT = 60  # Number of LED pixels.
LED_1_PIN = 13  # GPIO pin connected to the pixels (must support PWM! GPIO 13 and 18 on RPi 3).
LED_1_DMA = 11  # DMA channel to use for generating signal (Between 1 and 14)
LED_1_CHANNEL = 1  # 0 or 1

# LED strip configuration: LOWER STRIP
LED_2_COUNT = 100  # Number of LED pixels.
LED_2_PIN = 12  # GPIO pin connected to the pixels (must support PWM! GPIO 13 or 18 on RPi 3).
LED_2_DMA = 12  # DMA channel to use for generating signal (Between 1 and 14)
LED_2_CHANNEL = 0  # 0 or 1


# Main program logic follows:
if __name__ == '__main__':
    # Create NeoPixel objects with appropriate configuration for each strip.
    strip1 = led_wrapper.LEDWrapper(LED_1_COUNT, LED_1_PIN, LED_1_DMA, LED_1_CHANNEL)
    strip2 = led_wrapper.LEDWrapper(LED_2_COUNT, LED_2_PIN, LED_2_DMA, LED_2_CHANNEL)

    # Initialize the library (must be called once before other functions).
    strip1.begin()
    strip2.begin()

    print('Press Ctrl-C to quit.')

    # Black out any LEDs that may be still on for the last run
    strip1.blackout()
    strip2.blackout()
    strip1.show()
    strip2.show()

    # start communication
    fifo_path = "/tmp/communication.fifo"

    # check if not default path
    if len(sys.argv) > 1:
        fifo_path = sys.argv[1]

    # start named pipe if not exist
    if not os.path.exists(fifo_path):
        try:
            os.mkfifo(fifo_path)
            os.chmod(fifo_path, 666)
        except PermissionError:
            print("root-permissions necessary")
            sys.exit(1)

    fifo = open(fifo_path, 'r')

    try:
        while True:

            # data of LED: dataLED1 /n dataLED2
            upper_strip = fifo.readline()
            lower_strip = fifo.readline()

            # if pipe data ist empty -> quit
            if not upper_strip or not lower_strip:
                break

            upper_strip_split = upper_strip[:-1].split(",")
            lower_strip_split = lower_strip[:-1].split(",")

            # check obtained data length if not equal try again
            if len(upper_strip_split) > LED_1_COUNT or len(lower_strip_split) > LED_2_COUNT:
                print("Error: Unexpected data length!")
                continue

            # split the string into 32 bit integers and push it to the led_data structure
            for i, c in enumerate(upper_strip_split):
                strip1.set_pixel_color(i, int(c))

            for i, c in enumerate(lower_strip_split):
                strip2.set_pixel_color(i, int(c))

            strip1.show()
            strip2.show()
    except KeyboardInterrupt:
        pass

    # close and delete named pipe
    fifo.close()
    os.remove(fifo_path)

    strip1.blackout()
    strip2.blackout()
    strip1.show()
    strip2.show()
