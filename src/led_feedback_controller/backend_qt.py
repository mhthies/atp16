import threading

from PyQt5 import QtCore, QtGui, QtWidgets

from .colour import Colour


class GuiBackend:
    """
    A backend to show the colour values on screen for debugging purposes.
    """
    def __init__(self, argv):
        self.app = QtWidgets.QApplication(argv)
        self.widget = LEDWidget()

    def set_led_values(self, upper_strip, lower_strip):
        """
        Update the colour values to be shown on the LED strips.

        :param upper_strip: Colours to be shown on the upper LED strip. Should be a list of 100 Colour objects.
        :type upper_strip: [Colour]
        :param lower_strip: Colours to be shown on the lower LED strip. Should be a list of 100 Colour objects.
        :type lower_strip: [Colour]
        """
        self.widget.update_colours((upper_strip, lower_strip))

    def run(self):
        self.widget.show()
        return self.app.exec_()


class LEDWidget(QtWidgets.QWidget):
    requestUpdate = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setFixedSize(100*6+20-4, 2*20+20-4)
        self.setWindowTitle("LED Preview")
        p = QtGui.QPalette()
        p.setColor(QtGui.QPalette.Background, QtCore.Qt.black)
        self.setPalette(p)
        self.lock = threading.RLock()
        self.colours = ([], [])
        self.requestUpdate.connect(self.update)

    def update_colours(self, colours):
        with self.lock:
            self.colours = colours
        self.requestUpdate.emit()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        self.draw_leds(qp)
        qp.end()

    def draw_leds(self, qp):
        qp.setPen(QtCore.Qt.NoPen)
        with self.lock:
            for i, strip in enumerate(self.colours[:2]):
                for j, color in enumerate(strip[:100]):
                    c = Colour().blend(color)
                    qp.setBrush(QtGui.QColor(round(c.red*255), round(c.green*255), round(c.blue*255)))
                    qp.drawRect(j*6+10, i*20+10, 4, 4)
