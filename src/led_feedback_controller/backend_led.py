import sys
import logging
import os , time

import _thread

logger = logging.getLogger(__name__)


class LEDBackend:
    """
    A backend to pass colour values to an LED strip.
    """

    def __init__(self, pipe):
        """
        Construct a new LEDBackend.

        Wait until named pape exist.

        :param pipe: The path of the named pipe to pass the values to LED daemon.
        :type pipe: str
        """
        # wait until named pape is created
        while not os.path.exists(pipe):
            time.sleep(1)

        self.fifo = open(pipe, 'w')

    def set_led_values(self, upper_strip, lower_strip):
        """
        Update the colour values to be shown on the LED strips.

        :param upper_strip: Colours to be shown on the upper LED strip. Should be a list of 100 Colour objects.
        :type upper_strip: [Colour]
        :param lower_strip: Colours to be shown on the lower LED strip. Should be a list of 100 Colour objects.
        :type lower_strip: [Colour]
        """

        upper_strip_32bit = [str((int(round((colour.red * colour.alpha * 255)) << 16) |
                                 (int(round(colour.green * colour.alpha * 255)) << 8) |
                                 int(round(colour.blue * colour.alpha * 255))))
                             for colour in upper_strip]

        lower_strip_32bit = [str((int(round((colour.red * colour.alpha * 255)) << 16) |
                                 (int(round(colour.green * colour.alpha * 255)) << 8) |
                                 int(round(colour.blue * colour.alpha * 255))))
                             for colour in lower_strip]

        led_string = ",".join(upper_strip_32bit) + "\n" + ",".join(lower_strip_32bit) + "\n"

        try:
            self.fifo.write(led_string)
            self.fifo.flush()
        except Exception as e:
            logger.error("Error while writing into named pipe of LED daemon", exc_info=e)
            _thread.interrupt_main()
            sys.exit(1)


