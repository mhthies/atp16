import enum
import logging
import threading

import math

import copy

from .colour import Colour
import time


class PatternType(enum.Enum):
    OFF = 0
    STATIC = 1
    FADE = 2
    BLINK = 3
    NOVA = 4
    SLIDER = 5
    HEARTBEAT = 6
    WAVE = 7
    FLASH = 8


# setup the number LEDs of the strips
LED_NUM_UPPER = 60
LED_NUM_LOWER = 75

logger = logging.getLogger(__name__)


class PatternGenerator(threading.Thread):
    """
    Graphics component of the LED feedback controller. The PatternGenerator is informed about the colours, blink
    patterns and animations to be shown on the LED strips and renders the colours of each single LED in real time.
    """

    def __init__(self, backend):
        super().__init__(daemon=True)
        self._backend = backend

        self._upper_pattern_ts = time.time()
        self._upper_pattern_colour = Colour()
        self._upper_pattern_pattern = PatternType.STATIC
        self._upper_pattern_position = 1
        self._upper_pattern_speed = 1

        self._lower_pattern_ts = time.time()
        self._lower_pattern_colour = Colour()
        self._lower_pattern_pattern = PatternType.STATIC
        self._lower_pattern_position = 1
        self._lower_pattern_speed = 1

        self._upper_overlay_ts = time.time()
        self._upper_overlay_colour = Colour()
        self._upper_overlay_pattern = PatternType.OFF
        self._upper_overlay_position = 0
        self._upper_overlay_speed = 1
        self._upper_overlay_opacity = 1

        self._lower_overlay_ts = time.time()
        self._lower_overlay_colour = Colour()
        self._lower_overlay_pattern = PatternType.OFF
        self._lower_overlay_position = 0
        self._lower_overlay_speed = 1
        self._lower_overlay_opacity = 1

        self.framerate = 40  # in frames/s

    def set_upper_pattern(self, colour, pattern, position, speed):
        """
        Set a pattern to be shown on the upper LED strip.

        :param colour: The base colour of the displayed pattern
        :type colour: Colour
        :param pattern: The fade pattern/animation to be shown
        :type pattern: PatternType
        :param position: If the pattern allows/needs it: An emphasized position on the LED strip in [0..1]
        :type position: float
        :param speed: The speed of the animation
        :type speed: float
        """
        if self._upper_pattern_pattern != pattern:
            self._upper_pattern_ts = time.time()
        self._upper_pattern_colour = colour
        self._upper_pattern_pattern = pattern
        self._upper_pattern_position = position
        self._upper_pattern_speed = speed

    def set_upper_overlay(self, colour, pattern, position, speed, opacity=1):
        """
        Set an overlay pattern to be shown on the upper LED strip.

        :param colour: The base colour of the displayed pattern
        :type colour: Colour
        :param pattern: The fade pattern/animation to be shown
        :type pattern: PatternType
        :param position: If the pattern allows/needs it: An emphasized position on the LED strip in [0..1]
        :type position: float
        :param speed: The speed of the animation
        :type speed: float
        :param opacity: Opacity (alpha value) of the overlay over the basic pattern
        """
        if self._upper_overlay_pattern != pattern:
            self._upper_overlay_ts = time.time()
        self._upper_overlay_colour = colour
        self._upper_overlay_pattern = pattern
        self._upper_overlay_position = position
        self._upper_overlay_speed = speed
        self._upper_overlay_opacity = opacity

    def set_lower_pattern(self, colour, pattern, position, speed):
        """
        Set a pattern to be shown on the upper LED strip.

        :param colour: The base colour of the displayed pattern
        :type colour: Colour
        :param pattern: The fade pattern/animation to be shown
        :type pattern: PatternType
        :param position: If the pattern allows/needs it: An emphasized position on the LED strip in [0..1]
        :type position: float
        :param speed: The speed of the animation
        :type speed: float
        """
        if self._lower_pattern_pattern != pattern:
            self._lower_pattern_ts = time.time()
        self._lower_pattern_colour = colour
        self._lower_pattern_pattern = pattern
        self._lower_pattern_position = position
        self._lower_pattern_speed = speed

    def set_lower_overlay(self, colour, pattern, position, speed, opacity=1):
        """
        Set an overlay pattern to be shown on the upper LED strip.

        :param colour: The base colour of the displayed pattern
        :type colour: Colour
        :param pattern: The fade pattern/animation to be shown
        :type pattern: PatternType
        :param position: If the pattern allows/needs it: An emphasized position on the LED strip in [0..1]
        :type position: float
        :param speed: The speed of the animation
        :type speed: float
        :param opacity: Opacity (alpha value) of the overlay over the basic pattern
        """
        if self._lower_overlay_pattern != pattern:
            self._lower_overlay_ts = time.time()
        self._lower_overlay_colour = colour
        self._lower_overlay_pattern = pattern
        self._lower_overlay_position = position
        self._lower_overlay_speed = speed
        self._lower_overlay_opacity = opacity

    def run(self):
        while True:
            tic = time.time()
            try:
                self._calculate_frame()
            except Exception as e:
                logger.error("Error while generating pattern", exc_info=e)
                time.sleep(0.1)
            # Limit framerate
            time.sleep(max(0, 1 / self.framerate - (time.time() - tic)))

    def _calculate_frame(self):
        """
        Generate colours for eac LED and send them to the backend.
        """
        # Dict of led colour generation functions for each PatternType
        generators = {
            PatternType.STATIC: self._generate_static,
            PatternType.FADE: self._generate_fade,
            PatternType.WAVE: self._generate_wave,
            PatternType.BLINK: self._generate_blink,
            PatternType.HEARTBEAT: self._generate_heartbeat,
            PatternType.NOVA: self._generate_nova,
            PatternType.SLIDER: self._generate_slide,
            PatternType.FLASH: self._generate_flash
        }

        # Look up generator function for given pattern type to calculate the colours
        upper = generators[self._upper_pattern_pattern](self._upper_pattern_colour, self._upper_pattern_speed,
                                                        time.time() - self._upper_pattern_ts,
                                                        self._upper_pattern_position,
                                                        LED_NUM_UPPER)

        lower = generators[self._lower_pattern_pattern](self._lower_pattern_colour, self._lower_pattern_speed,
                                                        time.time() - self._lower_pattern_ts,
                                                        self._lower_pattern_position, LED_NUM_LOWER)

        if self._upper_overlay_pattern is not PatternType.OFF:
            upper_ol = generators[self._upper_overlay_pattern](self._upper_overlay_colour, self._upper_overlay_speed,
                                                               time.time() - self._upper_overlay_ts,
                                                               self._upper_overlay_position, LED_NUM_UPPER)
            # Calculate mixed colours from opacity, background list and overlay list
            upper = [bgc.blend(olc, self._upper_overlay_opacity)
                     for bgc, olc in zip(upper, upper_ol)]

        if self._lower_overlay_pattern is not PatternType.OFF:
            lower_ol = generators[self._lower_overlay_pattern](self._lower_overlay_colour, self._lower_overlay_speed,
                                                               time.time() - self._lower_overlay_ts,
                                                               self._lower_overlay_position, LED_NUM_LOWER)
            # Calculate mixed colours from opacity, background list and overlay list
            lower = [bgc.blend(olc, self._lower_overlay_opacity)
                     for bgc, olc in zip(lower, lower_ol)]

        self._backend.set_led_values(upper, lower)

    # #################################################################
    # methods for generating different patterns
    # #################################################################

    @staticmethod
    def _generate_static(color, speed, time_diff, position, led_num):
        """
        STATIC: all leds have the same color and brightness, over the time the brightness is constant, right of the
        defined position the leds are black
        """
        result = []
        for i in range(led_num):
            if i <= position * led_num:
                result.append(color)
            else:
                result.append(Colour(alpha=0))
        return result

    @staticmethod
    def _generate_fade(color, speed, time_diff, position, led_num):
        """
        FADE: all leds have the same color and brightness, over the time the brightness changes in a cycle with a
        defined speed
        """
        AMPLITUDE = 0.4
        c = copy.copy(color)
        c.value *= (1-AMPLITUDE) + AMPLITUDE * math.sin(-time_diff * speed)
        return [c] * led_num

    @staticmethod
    def _generate_wave(color, speed, time_diff, position, led_num):
        """
        WAVE: a wave is moving from left to right, the pattern fades out to the position. if the position is == 1, there
        is no fadeout.
        """
        AMPLITUDE = 0.4
        result = [copy.copy(color) for i in range(led_num)]
        if position < 1:
            FADEOUT_LEN = 0.1
        else:
            FADEOUT_LEN = 0

        for i, c in enumerate(result):
            c.value *= (1-AMPLITUDE) + AMPLITUDE * math.sin(-time_diff*speed + i/10)
            if i >= position * led_num:
                c.alpha = 0
            elif i >= ((position - FADEOUT_LEN) * led_num):
                c.alpha *= 1 / FADEOUT_LEN * (position - i / led_num)
        return result

    @staticmethod
    def _generate_slide(color, speed, time_diff, position, led_num):
        """
        SLIDE: A wave is moving from left to right, then from right to left; the cycle will be repeated until it is
        stopped
        """
        LEN = 0.1
        centerpos = math.sin(-time_diff * speed / 10)**2
        result = [copy.copy(color) for i in range(led_num)]
        for i, c in enumerate(result):
            c.alpha *= max(0, min(1, 3 - 3/LEN * abs(i/led_num - centerpos)))
        return result

    @staticmethod
    def _generate_blink(color, speed, time_diff, position, led_num):
        """BLINK: all leds are ON and OFF for a defined time; you can change the relation between ON and OFF (the
        dutycycle) with the THRESH parameter
        """
        THRESH = 0.4
        a = math.sin(speed * time_diff)
        return [color] * led_num if a > THRESH else [Colour(alpha=0)] * led_num

    @staticmethod
    def _generate_nova(color, speed, time_diff, position, led_num):
        """
        NOVA: two waves are moving from the bezels to the defined position; the waves are synchronised and vanish in
        the position
        """
        result = [copy.copy(color) for i in range(led_num)]
        for i, c in enumerate(result):
            if i <= position * led_num:
                c.alpha *= math.sin(time_diff * speed - i / 20)**8
            else:
                c.alpha *= math.sin(-(time_diff * speed - ((led_num-1)/20 - math.pi)) -
                                    ((i+((0.5-position)*2*led_num)) / 20))**8
        return result

    @staticmethod
    def _generate_heartbeat(color, speed, time_diff, position, led_num):
        """
        HEARTBEAT: Generates two bright pulses, then some delay, and repeats
        """
        BEAT_LENGTH = 0.7
        t = time_diff % (1/speed)
        c = copy.copy(color)
        if t < BEAT_LENGTH:
            c.alpha *= math.sin(t / BEAT_LENGTH * 2 * math.pi) ** 2
        else:
            c.alpha = 0
        return [c] * led_num


    @staticmethod
    def _generate_flash(color, speed, time_diff, position, led_num):
        c = copy.copy(color)
        c.alpha *= time_diff*speed*math.exp(1-time_diff*speed)
        return [c] * led_num
