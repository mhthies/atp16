
import argparse
import logging
import sys
import time

from led_feedback_controller import state_controller, pattern_generator, frontend_rest, backend_led , colour

if __name__ == "__main__":
    # Configure logging
    logging.basicConfig(level=logging.INFO)

    # Parse arguments
    parser = argparse.ArgumentParser(description="The main program to control the LED feedback display.")
    parser.add_argument("--mqtt", nargs=1, metavar="BROKER-ADDR", help="If specified, the MQTT frontend is started and"
                                                                       " connects to the given MQTT broker address.")
    parser.add_argument("--rest", nargs=1, type=int, metavar="PORT",
                        help="If specified, the REST frontend is started and listens on the given port.")
    parser.add_argument("--led-pipe", nargs=1, metavar="PIPE", default="/tmp/communication.fifo",
                        help="Specify the path of the named pipe, the LED daemon is listening on.")
    parser.add_argument("--gui", action='store_true', help="Start the PyQt Gui to show the LED colours instead of"
                                                           " connecting to the LED daemon.")
    parser.add_argument('args', nargs=argparse.REMAINDER)
    args = parser.parse_args()

    # Chose and setup backend
    if args.gui:
        # Local import to avoid PyQt dependency if not necessary
        from led_feedback_controller.backend_qt import GuiBackend
        backend = GuiBackend(args.args)
    else:
        backend = backend_led.LEDBackend(args.led_pipe)

    # Setup internal components
    pattern_gen = pattern_generator.PatternGenerator(backend)
    pattern_gen.start()
    state_con = state_controller.StateController(pattern_gen)

    # Setup and start frontends
    if args.rest:
        rest_fe = frontend_rest.RESTFrontend(args.rest[0], state_con)
        rest_fe.start()
    if args.mqtt:
        from led_feedback_controller.frontend_mqtt import MQTTFrontend
        mqtt_fe = MQTTFrontend(args.mqtt[0], state_con)
        mqtt_fe.start()

    # Start gui if required
    if args.gui:
        # Exec QApplication
        sys.exit(backend.run())
    else:
        # Wait for a KeyboardInterrupt
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            pass
