import cgi
import datetime
import json
import logging
import posixpath
import threading
import http.server
import urllib.parse

from . import state_controller

logger = logging.getLogger(__name__)


class RESTFrontend(threading.Thread):
    """
    A REST frontend for the LED feedback controller based on the Python http.server module.
    """

    def __init__(self, port, state_ctrl):
        super().__init__(daemon=True)
        self.port = port
        self.state_controller = state_ctrl

    def run(self):
        server_address = ('', self.port)
        logger.info("Starting REST server on {} …".format(server_address))
        endpoint_handler = LedFeedbackEndpointHandler(self.state_controller)
        httpd = http.server.HTTPServer(server_address, type('PrivateEndpointHandler', (RESTRequestHandler,),
                                                            {'endpoint_handler': endpoint_handler}))
        httpd.serve_forever()


class RESTRequestHandler(http.server.BaseHTTPRequestHandler):
    """
    A RequestHandler class to use the http.server as a REST server. GET and POST requests are forwarded to an instance
    of EndpointHandler, that can process the request parameters and generate a HTTP status code and a response object.
    The response is JSON encoded and sent to the client. Exceptions while processing the query are caught and result in
    an HTTP 500 response.

    As an instance of the RESTRequestHandler is created for each HTTP request by the http.server.HTTPServer, this class
    must be subclassed at runtime to provide an EndpointHandler instance dynamically::

        httpd = http.server.HTTPServer(address, type('EndPointHandlerName', (RESTRequestHandler,),
                                                     {'endpoint_handler': endpoint_handler}))
    """
    endpoint_handler = None

    def do_GET(self):
        """Serve a GET request by calling the endpoint_handler's get() method and JSON-encoding the result."""
        path, parameters = self._get_normalized_path()
        if not self.endpoint_handler.is_endpoint(path):
            self.send_error(http.server.HTTPStatus.NOT_FOUND, "Endpoint not found: {}".format(path))
            return

        try:
            http_code, response = self.endpoint_handler.get(path, parameters)
            response_raw = json.dumps(response).encode('utf-8')
        except Exception as e:
            logger.error("Error while handling GET request to {}:".format(path), exc_info=e)
            http_code = http.server.HTTPStatus.INTERNAL_SERVER_ERROR
            response_raw = json.dumps({'error': 'Internal server error: {}'.format(e)}).encode('utf-8')
        self._send(http_code, response_raw)

    def do_POST(self):
        """Serve a POST request by calling the endpoint_handler's post() method and JSON-encoding the result."""
        path, parameters = self._get_normalized_path()
        if not self.endpoint_handler.is_endpoint(path):
            self.send_error(http.server.HTTPStatus.NOT_FOUND, "Endpoint not found: {}".format(path))
            return

        try:
            http_code, response = self.endpoint_handler.post(path, parameters, self._get_post_values())
            response_raw = json.dumps(response).encode('utf-8')
        except Exception as e:
            logger.error("Error while handling POST request to {}:".format(path), exc_info=e)
            http_code = http.server.HTTPStatus.INTERNAL_SERVER_ERROR
            response_raw = json.dumps({'error': 'Internal server error: {}'.format(e)}).encode('utf-8')
        self._send(http_code, response_raw)

    def log_message(self, format, *args):
        logger.debug("HTTP request: " + format, *args)

    def _send(self, http_code, response):
        """
        Send a HTTP response with the given HTTP Statuscode and content to the client.

        :param http_code: The HTTP status code. Should be one of http.server.HTTPStatus.*
        :type: http_code: http.server.HTTPStatus
        :param response: The response body as a bytearray.
        :type response: bytes
        """
        self.send_response(http_code)
        self.send_header("Content-Type", "text/json; charset=utf-8")
        self.send_header("Content-Length", str(len(response)))
        self.end_headers()
        self.wfile.write(response)
        self.wfile.flush()

    def _get_normalized_path(self):
        """
        Get the normalized path and request parameters from the request headers. Path and parameters are stripped from
        local parameters (prefixed with '#') and URL-decoded.

        :return: The requested path and the request parameters
        :rtype: str, {str: [str]}
        """
        parts = self.path.split('?', 1)
        path = parts[0]
        parameters = parts[1] if len(parts) > 1 else ""
        # local query parameters
        path = path.split('#', 1)[0]
        parameters = parameters.split('#', 1)[0]
        try:
            path = urllib.parse.unquote(path, errors='surrogatepass')
        except UnicodeDecodeError:
            path = urllib.parse.unquote(path)
        path = posixpath.normpath(path)
        parameters = urllib.parse.parse_qs(parameters)
        return path, parameters

    def _get_post_values(self):
        """
        Extract the post values from the request headers.

        :return: A dict containing the post values as key/value pairs.
        :rtype: {str: [str]}
        """
        ctype, pdict = cgi.parse_header(self.headers.get('Content-Type'))
        charset = pdict['charset'] if 'charset' in pdict else 'ISO-8859-1'
        if ctype == 'multipart/form-data':
            pdict['boundary'] = bytes(pdict['boundary'], charset)
            postvars = {k: [x.decode(charset) for x in v]
                        for k, v in cgi.parse_multipart(self.rfile, pdict).items()}
        elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers.get('content-length'))
            postvars = urllib.parse.parse_qs(self.rfile.read(length).decode(charset), keep_blank_values=1)
        else:
            postvars = {}
        return postvars


class EndpointHandler:
    """
    Base class for EndpointHandlers to be used with the RESTRequestHandler. This class provides the basic interface to
    call handler functions for GET and POST requests. The handler function to be used is looked up in the `endpoints`
    dict based on the requested path.

    To provide handler functions, this class should be subclassed, so the subclasse's __init__-function can fill the
    `endpoints` dict. Each entry in the dict should be a tuple of two functions, the first is called for incoming GET
    requests with the dict of URL query parameters as the only argument, the second for POST requests, with two
    positional arguments: The URL query parameters and the form fields. Both of them should return an HTTPStatus and
    a response object to be JSON encoded and sent to the client.
    """

    def __init__(self):
        self.endpoints = {}

    def get(self, endpoint, parameters):
        """
        Handle an incoming GET request, by passing the request parameters to the endpoint's GET handler function.

        :param endpoint: The requested path (to chose the handler function)
        :type endpoint: str
        :param parameters: The parsed URL query parameters
        :type parameters: {str: [str]}
        :return: The HTTP Status code and the response object (to be JSON encoded)
        :rtype: http.server.HTTPStatus, dict
        """
        return self.endpoints[endpoint][0](parameters)

    def post(self, endpoint, parameters, form_data):
        """
        Handle an incoming POST request, by passing the request parameters and form data to the endpoint's POST
        handler function.

        :param endpoint: The requested path (to chose the handler function)
        :type endpoint: str
        :param parameters: The parsed URL query parameters
        :type parameters: {str: [str]}
        :param form_data: The parsed form data
        :type form_data: {str: [str]}
        :return: The HTTP Status code and the response object (to be JSON encoded)
        :rtype: http.server.HTTPStatus, dict
        """
        return self.endpoints[endpoint][1](parameters, form_data)

    def is_endpoint(self, endpoint):
        """
        Check, if the given endpoint exists.

        :param endpoint: The URL path of the assumed endpoint.
        :type endpoint: str
        :return: True if the endpoint exists (i.e. endpoint handlers are defined)
        :rtype: bool
        """
        return endpoint in self.endpoints


class LedFeedbackEndpointHandler(EndpointHandler):
    """
    A concrete EndpointHandler that provides HTTP endpoints and handler functions for the LED feedback controller. It
    defines handlers to update the machine and terminal state and trigger terminal feedback and bargraphs. The received
    state updates (and triggers) are passed to the given StateController.
    """

    def __init__(self, state_ctrl):
        """
        Create a new LedFeedbackEndpointHandler to be used with RESTRequestHandlers. The EndpointHandler will forward
        all received state updates to the given StateController.

        :param state_ctrl: The StateController to be updated by this REST interface.
        :type state_ctrl: state_controller.StateController
        """
        super().__init__()
        self.state_controller = state_ctrl

        self.endpoints['/machine/state'] = (self._get_machine_state, self._set_machine_state)
        self.endpoints['/terminal/state'] = (self._get_terminal_state, self._set_terminal_state)
        self.endpoints['/terminal/feedback'] = (self._get_terminal_feedback, self._trigger_terminal_feedback)
        self.endpoints['/terminal/bargraph'] = (self._get_terminal_bargraph, self._trigger_terminal_bargraph)
        self.endpoints['/ping'] = (self._ping, self._post_ping)

    def _get_machine_state(self, parameters):
        """
        GET request handler for the machine state endpoint. Returns the current machine state, known to the
        StateController.

        :param parameters: Unused.
        :return: HTTP 200 (should not fail), dict containing 'state', 'progress' and 'error_position'
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received GET request for machine state")
        return http.server.HTTPStatus.OK, \
               {'state': self.state_controller.machine_state.value, 'progress': self.state_controller.machine_progress,
                'error_position': self.state_controller.machine_error_position}

    def _set_machine_state(self, parameters, form_data):
        """
        POST request handler for the machine state endpoint. Updates the StateController's machine state with the given
        values.

        :param parameters: Unused.
        :param form_data: Form data from the POST request. Required parameters: 'state', optional parameters:
               'progress' and 'error_position'. If not given the optional parameters are set to 0.
        :return: HTTP 200 (if request is ok) or HTTP 422 (on parsing errors),
                 dict containing 'error' on parsing errors, else empty dict
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received POST request for machine state: {}".format(form_data))
        if 'state' not in form_data:
            logger.error("Machine state POST request is invalid: No 'state' provided.")
            return http.server.HTTPStatus.UNPROCESSABLE_ENTITY, {'error': "No 'state' provided."}

        try:
            state = state_controller.MachineStates(int(form_data['state'][0]))
            progress = float(form_data['progress'][0]) if 'progress' in form_data else 1.0
            error_position = float(form_data['error_position'][0]) if 'error_position' in form_data else 0.0
            self.state_controller.set_machine_state(state, progress, error_position)
        except ValueError as e:
            logger.error("Not a valid machine state: {}. Error: {}".format(form_data, e))
            return http.server.HTTPStatus.UNPROCESSABLE_ENTITY, {'error': "Error while parsing values: {}".format(e)}

        return http.server.HTTPStatus.OK, {}

    def _get_terminal_state(self, parameters):
        """
        GET request handler for the terminal state endpoint. Returns the current terminal state, known to the
        StateController.

        :param parameters: Unused.
        :return: HTTP 200 (should not fail), dict containing 'state', 'progress' and 'error_position'
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received GET request for terminal state")
        return http.server.HTTPStatus.OK, \
               {'state': self.state_controller.terminal_state, 'progress': self.state_controller.terminal_progress}

    def _set_terminal_state(self, parameters, form_data):
        """
        POST request handler for the terminal state endpoint. Updates the StateController's terminal state with the
        given values.

        :param parameters: Unused.
        :param form_data: Form data from the POST request. Required parameters: 'state', optional parameters:
               'progress'. If not given the optional parameters are set to 0.
        :return: HTTP 200 (if request is ok) or HTTP 422 (on parsing errors),
                 dict containing 'error' on parsing errors, else empty dict
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received POST request for terminal state: {}".format(form_data))
        if 'state' not in form_data:
            logger.error("Terminal state POST request is invalid: No 'state' provided.")
            return http.server.HTTPStatus.UNPROCESSABLE_ENTITY, {'error': "No 'state' provided."}

        try:
            state = state_controller.TerminalState(int(form_data['state'][0]))
            progress = float(form_data['progress'][0]) if 'progress' in form_data else 0.0
            self.state_controller.set_terminal_state(state, progress)
        except ValueError as e:
            logger.error("Not a valid terminal state: {}. Error: {}".format(form_data, e))
            return http.server.HTTPStatus.UNPROCESSABLE_ENTITY, {'error': "Error while parsing values: {}".format(e)}

        return http.server.HTTPStatus.OK, {}

    def _get_terminal_feedback(self, parameters):
        """
        GET request handler for the terminal feedback endpoint. Just returns a HTTP 405 "Method not allowed".

        :param parameters: Unused.
        :return: HTTP 405, dict containing an 'error'
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received GET request for terminal feedback")
        return http.server.HTTPStatus.METHOD_NOT_ALLOWED, \
               {'error': "Terminal feedback can only be triggered (via POST request), not be read."}

    def _trigger_terminal_feedback(self, parameters, form_data):
        """
        POST request handler for the terminal feedback endpoint. Triggers a terminal feedback at the StateController.

        :param parameters: Unused.
        :param form_data: Form data from the POST request. Optional parameter: 'type'. If not given, the feedback type
                          defaults to FeedbackTypes(1)
        :return: HTTP 200 (if request is ok) or HTTP 422 (on parsing errors),
                 dict containing 'error' on parsing errors, else empty dict
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received POST request for terminal feedback: {}".format(form_data))

        try:
            feedback_type = state_controller.FeedbackTypes(int(form_data['type'][0])) \
                            if 'type' in form_data else state_controller.FeedbackTypes(1)
            self.state_controller.trigger_terminal_feedback(feedback_type)
        except ValueError as e:
            logger.error("Not a valid terminal feedback: {}. Error: {}".format(form_data, e))
            return http.server.HTTPStatus.UNPROCESSABLE_ENTITY, {'error': "Error while parsing values: {}".format(e)}

        return http.server.HTTPStatus.OK, {}

    def _get_terminal_bargraph(self, parameters):
        """
        GET request handler for the terminal bargraph endpoint. Just returns a HTTP 405 "Method not allowed".

        :param parameters: Unused.
        :return: HTTP 405, dict containing an 'error'
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received GET request for terminal feedback")
        return http.server.HTTPStatus.METHOD_NOT_ALLOWED, \
               {'error': "Terminal bargraph can only be triggered (via POST request), not be read."}

    def _trigger_terminal_bargraph(self, parameters, form_data):
        """
        POST request handler for the terminal bargraph endpoint. Triggers a bargraph with the given value at the
        StateController.

        :param parameters: Unused.
        :param form_data: Form data from the POST request. Required parameter: 'value'
        :return: HTTP 200 (if request is ok) or HTTP 422 (on parsing errors),
                 dict containing 'error' on parsing errors, else empty dict
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received POST request for terminal bargraph: {}".format(form_data))
        if 'value' not in form_data:
            logger.error("Terminal bargraph POST request is invalid: No 'value' provided.")
            return http.server.HTTPStatus.UNPROCESSABLE_ENTITY, {'error': "No 'value' provided."}

        try:
            value = float(form_data['value'][0])
            self.state_controller.trigger_terminal_bargraph(value)
        except ValueError as e:
            logger.error("Not a valid terminal state: {}. Error: {}".format(form_data, e))
            return http.server.HTTPStatus.UNPROCESSABLE_ENTITY, {'error': "Error while parsing value: {}".format(e)}

        return http.server.HTTPStatus.OK, {}

    def _ping(self, parameters):
        """
        POST request handler for the ping endpoint. Just returns a HTTP 405 "Method not allowed".

        :param parameters: Unused.
        :return: HTTP 200, dict containing the current timestamp in ISO format at the key 'ts'
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received a Ping via REST.")
        return http.server.HTTPStatus.OK, \
               {'ts': datetime.datetime.now().isoformat()}

    def _post_ping(self, parameters, form_data):
        """
        POST request handler for the ping endpoint. Just returns a HTTP 405 "Method not allowed".

        :param parameters: Unused.
        :param form_data: Unused.
        :return: HTTP 405, dict containing an 'error'
        :rtype: http.server.HTTPStatus, dict
        """
        logger.debug("Received POST request for ping")
        return http.server.HTTPStatus.METHOD_NOT_ALLOWED, \
               {'error': "Terminal bargraph can only be triggered (via POST request), not be read."}