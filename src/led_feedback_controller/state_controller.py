import enum
import threading

import copy

from .pattern_generator import PatternType
from .colour import Colour
import logging

logger = logging.getLogger(__name__)

# Sets the timer for the display_timer for the overlay
BARGRAPH_TIME = 1.5
FEEDBACK_TIME = 0.8


class MachineStates(enum.Enum):
    """Here are the possible MachineStates listed, that the machine can be in"""
    OFF = 0
    READY = 1
    SLOW = 2
    RUNNING = 3
    WARNING = 4
    ERROR = 5
    CRITICAL = 6
    EXPLODING = 7


class TerminalState(enum.Enum):
    """Here are the possible TerminalStates listed"""
    OFF = 0
    READY = 1
    IN_USE = 2
    BUSY_BACKGROUND = 3
    BUSY = 4
    REQUESTING_ATTENTION = 5


class FeedbackTypes(enum.Enum):
    """
    Here are all possible FeedbackTypes listed
    """

    NO = 0
    TOUCH = 1


# Lists all color combinations for the LED strip. The are calculated by the (RBG_value/ 255)
BASIC_COLOURS = {
    'Light_Green': Colour(0.329, 1, 0.623),
    'Green_2': Colour(0.05, 0.8, 0.05),
    'Green': Colour(0.05, 1, 0.05),
    'Yellow': Colour(1, 1, 0),
    'Red': Colour(1, 0, 0),
    'Light_Red': Colour(0.85, 0.05, 0),
    'Blue_Fade': Colour(0, 0.2, 1),
    'Light_Blue': Colour(0.678, 0.847, 1),
    'White': Colour(1, 1, 1),
    'Black': Colour(0.0, 0.0, 0.0),
}


class StateController:
    """
    A main component of the LED feedback controller, that keeps track of the state of our machine and UI-terminal and
    calculates colours and blink patterns for our LED strips according to this state.

    The state is updated by the frontend components via the set_*() methods.
    """

    def __init__(self, pattern_generator):
        """
        Construct a new StateController.

        :param pattern_generator: The PatternGenerator to be controlled by this StateController
        :type pattern_generator: pattern_generator.PatternGenerator
        """
        self._pattern_generator = pattern_generator

        self.machine_state = MachineStates.OFF
        self.machine_progress = 0.0
        self.machine_error_position = 0.0

        self.terminal_state = TerminalState.OFF
        self.terminal_progress = 0.0

        self._terminal_bargraph = None
        self._terminal_bargraph_timer = threading.Timer(0, lambda x: None)
        self._terminal_feedback = None
        self._terminal_feedback_timer = threading.Timer(0, lambda x: None)

    def _update_upper_strip(self):
        """
        Calculate the colour, blink pattern and overlay action of the upper LED strip according to the current machine
        and terminal states and pass it to the PatternGenerator.
        """

        """ 
        Establish the default values of the upper strip for normal and overlay patterns.
        """
        colour = BASIC_COLOURS['Black']
        pattern = PatternType.STATIC
        position = 1
        speed = 5

        ol_colour = BASIC_COLOURS['Black']
        ol_pattern = PatternType.OFF
        ol_position = 1
        ol_speed = 5

        """
        Checks if there is a Error State. If not, checks if a overlay for feedback or bargraph is needed. 
        Else the terminal state will be displayed on the upper stripe. 
        """
        if self.machine_state in (MachineStates.ERROR, MachineStates.CRITICAL, MachineStates.EXPLODING):
            if self.machine_state == MachineStates.ERROR:
                pattern = PatternType.BLINK
                colour = BASIC_COLOURS['Light_Red']
                speed = 5
            elif self.machine_state == self.machine_state.CRITICAL:
                pattern = PatternType.BLINK
                colour = BASIC_COLOURS['Red']
                speed = 15
            elif self.machine_state == self.machine_state.EXPLODING:
                pattern = PatternType.NOVA
                colour = BASIC_COLOURS['Red']
                position = 0.5
                speed = -10
        else:
            if self.terminal_state == self.terminal_state.READY:
                colour = BASIC_COLOURS['Blue_Fade']
                pattern = PatternType.FADE
            elif self.terminal_state == self.terminal_state.IN_USE:
                colour = BASIC_COLOURS['Light_Green']
                pattern = PatternType.WAVE

            elif self.terminal_state == self.terminal_state.BUSY_BACKGROUND:
                ol_colour = BASIC_COLOURS['Yellow']
                colour = copy.copy(BASIC_COLOURS['Light_Green'])
                colour.value *= 0.5
                if self.terminal_progress != 0:
                    ol_pattern = PatternType.WAVE
                    ol_position = self.terminal_progress
                else:
                    ol_speed = 8
                    ol_pattern = PatternType.SLIDER
            elif self.terminal_state == self.terminal_state.BUSY:
                colour = copy.copy(BASIC_COLOURS['Blue_Fade'])
                colour.value *= 0.1
                ol_colour = BASIC_COLOURS['Yellow']
                if self.terminal_progress != 0:
                    ol_pattern = PatternType.WAVE
                    ol_position = self.terminal_progress
                    ol_speed = 8
                else:
                    ol_speed = 15
                    ol_pattern = PatternType.SLIDER
            elif self.terminal_state == self.terminal_state.REQUESTING_ATTENTION:
                colour = BASIC_COLOURS['Light_Blue']
                pattern = PatternType.HEARTBEAT
                speed = 0.4
            else:
                colour = BASIC_COLOURS['Black']
                pattern = PatternType.STATIC
        if self._terminal_bargraph is not None:
            ol_colour = BASIC_COLOURS['White']
            ol_position = self._terminal_bargraph
            ol_pattern = PatternType.STATIC
            colour = copy.copy(colour)
            colour.value *= 0.6
        if self._terminal_feedback is not None:
            ol_colour = BASIC_COLOURS['White']
            ol_pattern = PatternType.FLASH
            ol_speed = 15
        self._pattern_generator.set_upper_pattern(colour, pattern, position, speed)
        self._pattern_generator.set_upper_overlay(ol_colour, ol_pattern, ol_position, ol_speed, 1)

    def _update_lower_strip(self):
        """
        Calculate the colour, blink pattern and overlay action of the lower LED strip according to the current machine
        and terminal states and pass it to the PatternGenerator.
        """
        colour = BASIC_COLOURS['Black']
        pattern = PatternType.STATIC
        position = 1
        speed = 5

        ol_colour = BASIC_COLOURS['Black']
        ol_pattern = PatternType.OFF
        ol_position = 1
        ol_speed = 5

        if self.machine_state in (MachineStates.ERROR, MachineStates.CRITICAL, MachineStates.EXPLODING,
                                  MachineStates.WARNING):
            """Checks if a overlay needs to be implemented"""
            if self.machine_error_position is not None and self.machine_error_position != 0.0:
                ol_pattern = PatternType.NOVA
                ol_position = self.machine_error_position
                ol_colour = BASIC_COLOURS['White']
                ol_speed = 3

            if self.machine_state == self.machine_state.ERROR:
                colour = BASIC_COLOURS['Light_Red']
                pattern = PatternType.BLINK
                speed = 5
            elif self.machine_state == self.machine_state.CRITICAL:
                colour = BASIC_COLOURS['Red']
                pattern = PatternType.BLINK
                speed = 15
            elif self.machine_state == self.machine_state.EXPLODING:
                colour = BASIC_COLOURS['Red']
                pattern = PatternType.NOVA
                position = 0.5
                speed = -10
            else:
                colour = BASIC_COLOURS['Yellow']
                pattern = PatternType.WAVE
                speed = 9
        elif self.machine_state == self.machine_state.RUNNING:
            colour = BASIC_COLOURS['Green']
            pattern = PatternType.WAVE
            speed = 8
            position = self.machine_progress
        elif self.machine_state == self.machine_state.SLOW:
            colour = BASIC_COLOURS['Green']
            pattern = PatternType.WAVE
            speed = 3
            position = self.machine_progress
        elif self.machine_state == self.machine_state.READY:
            colour = BASIC_COLOURS['Green_2']
            pattern = PatternType.FADE
            position = self.machine_progress
            speed = 3
        else:
            position = self.machine_progress

        self._pattern_generator.set_lower_pattern(colour, pattern, position, speed)
        self._pattern_generator.set_lower_overlay(ol_colour, ol_pattern, ol_position, ol_speed, 1)

    def set_machine_state(self, state, progress, error_position):
        """Updates the values and LED Pattern if the machine_state changed"""
        logger.info("New machine state: {} -- {} -- {}".format(state, progress, error_position))
        if self.machine_state != state or self.machine_progress != progress or\
                self.machine_error_position != error_position:
            self.machine_state = state
            self.machine_progress = progress
            self.machine_error_position = error_position
            self._update_lower_strip()
            self._update_upper_strip()

    def set_terminal_state(self, state, progress):
        """Updates the values and LED Pattern if the terminal_state changed"""
        logger.info("New terminal state: {} -- {}".format(state, progress))
        if self.terminal_state != state or self.terminal_progress != progress:
            self.terminal_state = state
            self.terminal_progress = progress
            self._update_upper_strip()

    def trigger_terminal_feedback(self, feedback_type):
        """Implements the timer that allows the feedback to be displayed for the user"""
        logger.info("Terminal feedback triggered, type {}".format(feedback_type))
        self._terminal_feedback = feedback_type
        self._terminal_feedback_timer.cancel()
        self._terminal_feedback_timer = threading.Timer(FEEDBACK_TIME, self._reset_terminal_feedback)
        self._terminal_feedback_timer.start()
        self._update_upper_strip()

    def _reset_terminal_feedback(self):
        """Resets the feedback timer"""
        logger.debug("Terminal feedback reset")
        self._terminal_feedback = None
        self._update_upper_strip()

    def trigger_terminal_bargraph(self, value):
        """Implements the timer that allows the feedback to be displayed for the user"""
        logger.info("Terminal bargraph triggered, value {}".format(value))
        self._terminal_bargraph = value
        self._terminal_bargraph_timer.cancel()
        self._terminal_bargraph_timer = threading.Timer(BARGRAPH_TIME, self._reset_terminal_bargraph)
        self._terminal_bargraph_timer.start()
        self._update_upper_strip()

    def _reset_terminal_bargraph(self):
        """Resets the bargraph timer"""
        logger.debug("Terminal bargraph reset")
        self._terminal_bargraph = None
        self._update_upper_strip()
