import math


class Colour:
    """
    Class to represent RGB and HSV colours. The RGB values are stored natively in the `red`, `green` and `blue`
    variables; the HSV values can be accessed using the `hue`, `saturation` and `value` properties, that calculate the
    resulting channel value or RGB colour on the fly.
    """

    def __init__(self, red=0.0, green=0.0, blue=0.0, alpha=1.0, hue=None, saturation=None, value=None):
        """
        Construct a new Colour from RGB or HSV values.

        If any of hue, saturation or value is defined, the HSV values are used to initialize the colour. Otherwise, the
        RGB values are set. All values must be in range 0..1.

        :param red: RGB value red
        :type red: float
        :param green: RGB value green
        :type green: float
        :param blue: RGB value blue
        :type blue: float
        :param alpha: Alpha (opacity) of the colour
        :type alpha: float
        :param hue: HSV value hue
        :type hue: float
        :param saturation: HSV value saturation
        :type saturation: float
        :param value: HSV value value (brightness)
        :type value: float
        """
        self.red = red
        self.green = green
        self.blue = blue
        self.alpha = alpha

        if hue is not None or saturation is not None or value is not None:
            self._set_hsv(hue if hue is not None else 0,
                          saturation if saturation is not None else 0,
                          value if value is not None else 0)

    def _set_hsv(self, hue, saturation, value):
        # Based on https://gist.github.com/mjackson/5311256
        i = math.floor(hue * 6)
        f = hue * 6 - i
        p = value * (1 - saturation)
        q = value * (1 - f * saturation)
        t = value * (1 - (1-f) * saturation)
        if i == 0:
            self.red = value; self.green = t; self.blue = p
        elif i == 1:
            self.red = q; self.green = value; self.blue = p
        elif i == 2:
            self.red = p; self.green = value; self.blue = t
        elif i == 3:
            self.red = p; self.green = q; self.blue = value
        elif i == 4:
            self.red = t; self.green = p; self.blue = value
        else:  # i == 5
            self.red = value; self.green = p; self.blue = q

    def _get_hue(self):
        max_rgb = max(self.red, self.green, self.blue)
        min_rgb = min(self.red, self.green, self.blue)
        if max_rgb == min_rgb:
            return 0
        elif max_rgb == self.red:
            return 1/6 * (0 + (self.green - self.blue)/(max_rgb - min_rgb))
        elif max_rgb == self.green:
            return 1/6 * (2 + (self.blue - self.red)/(max_rgb - min_rgb))
        else:
            return 1/6 * (4 + (self.red - self.green)/(max_rgb - min_rgb))

    def _set_hue(self, hue=0.0):
        max_rgb = max(self.red, self.green, self.blue)
        min_rgb = min(self.red, self.green, self.blue)
        if max_rgb == 0 or max_rgb == min_rgb:
            return
        # Changing hue is just as complicated as setting all hsv values. So … we avoid duplicate code
        self._set_hsv(hue, (max_rgb - min_rgb) / max_rgb, max_rgb)

    def _get_saturation(self):
        max_rgb = max(self.red, self.green, self.blue)
        min_rgb = min(self.red, self.green, self.blue)
        if max_rgb == 0:
            return 0
        else:
            return (max_rgb - min_rgb) / max_rgb

    def _set_saturation(self, saturation=0.0):
        max_rgb = max(self.red, self.green, self.blue)
        min_rgb = min(self.red, self.green, self.blue)
        if max_rgb == 0:
            return
        cur_saturation = (max_rgb - min_rgb) / max_rgb
        if cur_saturation == 0:
            self.green -= saturation * max_rgb
            self.blue -= saturation * max_rgb
        else:
            self.red = max_rgb - (max_rgb - self.red) * saturation / cur_saturation
            self.green = max_rgb - (max_rgb - self.green) * saturation / cur_saturation
            self.blue = max_rgb - (max_rgb - self.blue) * saturation / cur_saturation

    def _get_value(self):
        return max(self.red, self.green, self.blue)

    def _set_value(self, value=0.0):
        cur_value = max(self.red, self.green, self.blue)
        if cur_value == 0:
            self.red = value
            self.green = value
            self.blue = value
        else:
            self.red *= value/cur_value
            self.green *= value/cur_value
            self.blue *= value/cur_value

    hue = property(_get_hue, _set_hue)
    saturation = property(_get_saturation, _set_saturation)
    value = property(_get_value, _set_value)

    def blend(self, other, alpha=1.0):
        """
        Blend the other colour into this colour and return the resulting colour.

        This method takes the native alpha values of the two colours into account while mixing the colours. It
        calculates the resulting colour and its alpha value, such that associativity holds:
        a.blend(b).blend(c) == a.blend(b.blend(c))

        :param other: The other colour
        :type other: Colour
        :param alpha: Opacity prescaler of the other colour. The native opacity of the other colour is multiplied with
                      the given alpha value before calculating the mixed colour.
        :type alpha: float
        :return: The resulting Colour
        :rtype: Colour
        """
        other_alpha = other.alpha * alpha

        # The formulas for alpha_new and mix_rgb() result from expanding (1-β)*((1-α)*a + α*b) + β*c and searching the
        # coefficient of 'a' (which is alpha_new) and dividing the rest by alpha_new (which is the new mixed colour
        # value)
        alpha_new = 1 - (1-self.alpha)*(1-other_alpha)
        def mix_rgb(own_value, other_value):
            return ((1 - other_alpha) * self.alpha * own_value + other_alpha * other_value) / alpha_new

        if alpha_new == 0:
            # Regular calculations will result in div-by-0 error in this case
            return Colour(0.0, 0.0, 0.0, 0.0)

        return Colour(mix_rgb(self.red, other.red),
                      mix_rgb(self.green, other.green),
                      mix_rgb(self.blue, other.blue),
                      alpha_new)

    def __repr__(self):
        return "Colour({}, {}, {}, {})".format(self.red, self.green, self.blue, self.alpha)

    def __str__(self):
        return "({}, {}, {}, {})".format(self.red, self.green, self.blue, self.alpha)
