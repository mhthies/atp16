import http.client
import queue
import threading
import urllib.parse
import json
import logging

import paho.mqtt.client as mqtt

from led_feedback_controller.frontend_mqtt import MQTT_TOPIC

logger = logging.getLogger(__name__)


class LEDClient:
    def __init__(self):
        # Setup MQTT client
        self.mqtt_client = mqtt.Client()
        self.mqtt_connected = False

        # Setup on_connect and on_message handler of the MQTT client
        def on_mqtt_connnect(client, userdata, flags, rc):
            if rc == 0:
                self.mqtt_connected = True
                client.subscribe("{}/pong".format(MQTT_TOPIC))
            else:
                logger.warning("MQTT connection could not be established: {}".format(mqtt.connack_string(rc)))
        self.mqtt_client.on_connect = on_mqtt_connnect

        def on_mqtt_message(client, userdata, message):
            if message.topic == "{}/pong".format(MQTT_TOPIC):
                # Set the ping_pong_event, to notify a waiting ping() method
                self.ping_pong_event.set()
        self.mqtt_client.on_message = on_mqtt_message

        # Set default connection settings
        self.rest_settings = (False, "", 0)  # (active, host, port)
        self.mqtt_settings = (False, "")  # (active, broker)

        # Some locking and communication mechanisms for the ping method
        self.ping_lock = threading.RLock()
        self.ping_pong_event = threading.Event()

        # And some asynchronous queue for the REST client
        self.rest_queue = queue.Queue()

    def send_machine_state(self, state=0, progress=0.0, error_position=0.0):
        if self.rest_settings[0]:
            self._send_rest_message("/machine/state", {'state': str(int(state)),
                                                       'progress': str(progress),
                                                       'error_position': error_position})
        if self.mqtt_settings[0]:
            self._send_mqtt_message("/machine/state", "{} {} {}".format(int(state), progress, error_position))

    def send_terminal_state(self, state=0, progress=0.0):
        if self.rest_settings[0]:
            self._send_rest_message("/terminal/state", {'state': str(int(state)),
                                                        'progress': str(progress)})
        if self.mqtt_settings[0]:
            self._send_mqtt_message("/terminal/state", "{} {}".format(int(state), progress))

    def trigger_terminal_feedback(self, feedback_type=0):
        if self.rest_settings[0]:
            self._send_rest_message("/terminal/feedback", {'feedback_type': str(int(feedback_type))})
        if self.mqtt_settings[0]:
            self._send_mqtt_message("/terminal/feedback", str(int(feedback_type)))

    def trigger_terminal_bargraph(self, value=0.0):
        if self.rest_settings[0]:
            self._send_rest_message("/terminal/bargraph", {'value': str(value)})
        if self.mqtt_settings[0]:
            self._send_mqtt_message("/terminal/bargraph", str(value))

    def set_connection_settings(self, settings):
        """
        Update the connection settings to reach the REST server and the MQTT broker.

        :param settings: A tuple of two tuples: ((RESTactive, RESThost, RESTport), (MQTTactive, MQTThost))
        :type settings: ((bool, str, int), (bool, str))
        """
        rest_settings, mqtt_settings = settings

        # REST client is stateless, but we have to start the background thread that does the sending
        self.rest_settings = rest_settings
        if self.rest_settings[0]:
            send_thread = threading.Thread(target=self._rest_send_thread, daemon=True)
            send_thread.start()

        # MQTT client must be reconnected for new broker settings to have effect
        if mqtt_settings != self.mqtt_settings:
            if self.mqtt_settings[0]:
                self.mqtt_connected = False
                self.mqtt_client.loop_stop()
                self.mqtt_client.disconnect()
            if mqtt_settings[0]:
                self.mqtt_client.connect_async(mqtt_settings[1])
                self.mqtt_client.loop_start()
            self.mqtt_settings = mqtt_settings

    def ping(self, timeout=2.0):
        """
        Try to ping the LED Feedback controller to check if the network connection is okay and the controller
        application is responsive.

        This method blocks until a response is received or the timeout expires. This method can only be executed once at
        the same time – otherwise all subsequent calls wait for the first one to finish.

        :param timeout: How long to wait for the response (in seconds)
        :type timeout: float
        :return: A tuple of success and the error message
        :rtype: (bool, str)
        """
        with self.ping_lock:
            # REST ping
            if self.rest_settings[0]:
                conn = http.client.HTTPConnection(self.rest_settings[1], self.rest_settings[2], timeout=timeout)
                try:
                    conn.request("GET", "/ping")
                except ConnectionError as e:
                    return False, "Connect error: {}".format(str(e))
                response = conn.getresponse()
                if response.status != 200:
                    return False, "HTTP response is not 200 but {}".format(response.status)
                return True, ""

            # MQTT ping: send ping, sleep on event until pong is received
            elif self.mqtt_settings[0]:
                if not self.mqtt_connected:
                    return False, "MQTT client not connected to broker"
                self.ping_pong_event.clear()
                self._send_mqtt_message("/ping", "")
                res = self.ping_pong_event.wait(timeout=timeout)
                if res:
                    return True, ""
                else:
                    return False, "MQTT ping timeout"
            else:
                return False, "Neither REST nor MQTT client active"

    def _send_mqtt_message(self, sub_topic, msg):
        """
        Send the given message to the given MQTT subtopic (which is appended to MQTT_TOPIC) at the currently configured
        MQTT broker.

        :param sub_topic: The subtopic, e.g. "/machine/state".
        :type sub_topic: str
        :param msg: The message string. It is utf8-encoded for transmission
        :type msg: str
        """
        if self.mqtt_connected:
            logging.debug("Publishing MQTT message to topic {}{}: {}".format(MQTT_TOPIC, sub_topic, msg))
            self.mqtt_client.publish("{}{}".format(MQTT_TOPIC, sub_topic), msg.encode('utf-8'))

    def _send_rest_message(self, endpoint, data):
        """
        Send a POST request to the given endpoint of the REST server, currently configured, with the given form data.
        The request is added to the rest_queue to be executed by the background REST send thread.

        :param endpoint: Endpoint to POST to, i.e. path port of the HTTP URL, e.g. "/machine/state"
        :type endpoint: str
        :param data: Form data to attach to the POST request
        :type data: {str: str}
        """
        if not self.rest_settings[0]:
            return
        self.rest_queue.put((endpoint, data))

    def _rest_send_thread(self):
        """
        Entry point for the REST background send thread. It runs in a loop until the rest_settings declare the rest
        client deactivated and takes single messages from the rest_queue, sends them and logs the response on failure.
        """
        while self.rest_settings[0]:
            try:
                endpoint, data = self.rest_queue.get(timeout=1.0)
            except queue.Empty:
                continue

            active, host, port = self.rest_settings
            if not active:
                with self.rest_queue.mutex:
                    self.rest_queue.queue.clear()
                break

            logging.debug("Send HTTP POST request to {}:{}{} with data: {}".format(host, port, endpoint, data))
            params = urllib.parse.urlencode(data)
            headers = {"Content-type": "application/x-www-form-urlencoded"}
            conn = http.client.HTTPConnection(host, port)
            try:
                conn.request("POST", endpoint, params, headers)
            except ConnectionError as e:
                logger.error("Could not connect to REST server at {}:{}: {}".format(host, port, str(e)))
                continue

            response = conn.getresponse()
            conn.close()
            if response.status != 200:
                try:
                    result = json.loads(response.read().decode())
                    error_message = result["error"]
                    logger.error("POST request to {}:{}{} failed with HTTP {}. Error message: {}"
                                 .format(host, port, endpoint, response.status, error_message))
                except json.JSONDecodeError:
                    logger.error("POST request to {}:{}{} failed with HTTP {}. Also, the response could not be "
                                 "JSON-parsed."
                                 .format(host, port, endpoint, response.status))
                except KeyError:
                    logger.error("POST request to {}:{}{} failed with HTTP {}. Also, the response does not contain an "
                                 "error message."
                                 .format(host, port, endpoint, response.status))
