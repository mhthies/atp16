import enum
import threading
import logging
from PyQt5 import QtCore

from led_feedback_controller.state_controller import MachineStates, TerminalState

logger = logging.getLogger(__name__)


class ScriptPlayer(QtCore.QObject):
    progress_changed = QtCore.pyqtSignal(tuple)

    def __init__(self, client):
        super().__init__()
        self.client = client

        self.step = 0
        self.script = []
        self.started = False

        self.timer = threading.Timer(0, lambda: None)

    def set_script(self, script):
        self.stop()
        self.step = 0
        self.script = script
        self.progress_changed.emit(self.get_progress())
        logger.info("New script with {} events for script player and total wait time of {}s".format(
            len(script), sum(event.time for event in script if event.type == ScriptEvent.Type.WAIT)))

    def start(self):
        self.started = True
        if self.step >= len(self.script):
            self.step = 0
        self.timer = threading.Timer(0, self._play)
        self.timer.setDaemon(True)
        self.timer.start()
        self.progress_changed.emit(self.get_progress())
        logger.info("Started script player")

    def stop(self):
        self.started = False
        self.timer.cancel()
        self.progress_changed.emit(self.get_progress())
        logger.info("Stopped script player at step {}".format(self.step))

    def _play(self):
        while self.started:
            step = self.step
            if step >= len(self.script):
                self.stop()
                return
            logger.debug("Script step {}".format(step))
            event = self.script[step]
            self.step += 1
            self.progress_changed.emit(self.get_progress())
            if event.type == ScriptEvent.Type.WAIT:
                logger.debug("Starting timer for {}s".format(event.time))
                self.timer = threading.Timer(event.time, self._play)
                self.timer.setDaemon(True)
                self.timer.start()
                return
            elif event.type == ScriptEvent.Type.MSTATE:
                logger.debug("Processing Machine State Event")
                self.client.send_machine_state(event.state.value, event.progress, event.error_position)
            elif event.type == ScriptEvent.Type.TSTATE:
                logger.debug("Processing Terminal State Event")
                self.client.send_terminal_state(event.state.value, event.progress)
            step = self.step

    def get_progress(self):
        step_time = 0
        total_time = 0
        step = self.step
        for i, e in enumerate(self.script):
            if e.type == ScriptEvent.Type.WAIT:
                total_time += e.time
                if i < step-1:
                    step_time += e.time
        return self.started, step, len(self.script), step_time, total_time


class ScriptEvent:
    class Type(enum.Enum):
        NONE = 0
        WAIT = 1
        MSTATE = 2
        TSTATE = 3

    type = None


class WaitEvent(ScriptEvent):
    type = ScriptEvent.Type.WAIT

    def __init__(self, time=0.0):
        self.time = time


class MStateEvent(ScriptEvent):
    type = ScriptEvent.Type.MSTATE

    def __init__(self, state, progress=1.0, error_position=0.0):
        self.state = state
        self.progress = progress
        self.error_position = error_position


class TStateEvent(ScriptEvent):
    type = ScriptEvent.Type.TSTATE

    def __init__(self, state, progress=1):
        self.state = state
        self.progress = progress


SCRIPTS = [
    ("Start Machine", [
        MStateEvent(MachineStates.OFF),
        WaitEvent(2),
        MStateEvent(MachineStates.READY),
        WaitEvent(2),
        MStateEvent(MachineStates.SLOW, progress=0),
        WaitEvent(0.2),
        MStateEvent(MachineStates.SLOW, progress=0.1),
        WaitEvent(0.2),
        MStateEvent(MachineStates.SLOW, progress=0.2),
        WaitEvent(0.2),
        MStateEvent(MachineStates.SLOW, progress=0.3),
        WaitEvent(0.2),
        MStateEvent(MachineStates.SLOW, progress=0.4),
        WaitEvent(0.2),
        MStateEvent(MachineStates.SLOW, progress=0.6),
        WaitEvent(0.2),
        MStateEvent(MachineStates.SLOW, progress=0.7),
        WaitEvent(1),
        MStateEvent(MachineStates.SLOW, progress=0.9),
        WaitEvent(3),
        MStateEvent(MachineStates.SLOW, progress=1),
        WaitEvent(0.3),
        MStateEvent(MachineStates.RUNNING),
        WaitEvent(5),
        MStateEvent(MachineStates.WARNING),
        WaitEvent(3),
        MStateEvent(MachineStates.WARNING, error_position=0.7),
        WaitEvent(5),
        MStateEvent(MachineStates.ERROR, error_position=0.7),
        WaitEvent(5),
        MStateEvent(MachineStates.OFF),
    ]),
]
