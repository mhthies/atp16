import logging
import sys
from PyQt5 import QtCore, QtGui, QtWidgets

from led_test_gui import widgets, led_client, scripts


class LEDTestWindow(QtWidgets.QMainWindow):
    def __init__(self, client, parent=None):
        super().__init__(parent)
        self.resize(600, 350)
        self.setWindowTitle("LED Feedback Test GUI")
        self.client = client
        self.script_player = scripts.ScriptPlayer(client)
        self._init_gui()

    def _init_gui(self):
        l1 = QtWidgets.QVBoxLayout()
        cw = QtWidgets.QWidget()
        self.setCentralWidget(cw)
        cw.setLayout(l1)

        l2 = QtWidgets.QHBoxLayout()
        l1.addLayout(l2)

        l2.addWidget(widgets.MachineStateWidget(client=client))
        l2.addWidget(widgets.TerminalStateWidget(client=client))
        l1.addWidget(widgets.ScriptPlayerWidget(self.script_player))

        l1.addStretch()
        conn_widget = widgets.ConnectionWidget()
        conn_widget.settingsChanged.connect(client.set_connection_settings)
        l1.addWidget(conn_widget)

        client.set_connection_settings((conn_widget.get_rest_connection(), conn_widget.get_mqtt_connection()))


if __name__ == "__main__":
    # Configure logging
    logging.basicConfig(level=logging.INFO)

    app = QtWidgets.QApplication(sys.argv)
    client = led_client.LEDClient()
    window = LEDTestWindow(client=client)
    window.show()
    app.exec_()
