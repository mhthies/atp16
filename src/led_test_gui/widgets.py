
from PyQt5 import QtCore, QtGui, QtWidgets
from led_feedback_controller.state_controller import MachineStates, TerminalState
from . import scripts


class ConnectionWidget(QtWidgets.QGroupBox):
    settingsChanged = QtCore.pyqtSignal(tuple)

    def __init__(self, parent=None):
        super().__init__("Connection", parent)
        self._init_gui()

    def _init_gui(self):
        l1 = QtWidgets.QHBoxLayout()
        self.setLayout(l1)

        l2 = QtWidgets.QVBoxLayout()
        l1.addLayout(l2)
        self.radio_REST = QtWidgets.QRadioButton("REST")
        self.radio_REST.setChecked(True)
        self.radio_REST.toggled.connect(self._on_radio_select)
        self.radio_MQTT = QtWidgets.QRadioButton("MQTT")
        l2.addWidget(self.radio_REST)
        l2.addWidget(self.radio_MQTT)

        l3 = QtWidgets.QFormLayout()
        l1.addLayout(l3)
        self.edit_rest_host = QtWidgets.QLineEdit("127.0.0.1")
        self.edit_rest_port = QtWidgets.QLineEdit("8000")
        self.edit_rest_port.setValidator(QtGui.QIntValidator(0, 2**16))
        self.edit_rest_host.textChanged.connect(self._on_rest_edit_change)
        self.edit_rest_port.textChanged.connect(self._on_rest_edit_change)
        l3.addRow("REST Host", self.edit_rest_host)
        l3.addRow("REST Port", self.edit_rest_port)

        l4 = QtWidgets.QFormLayout()
        l1.addLayout(l4)
        self.edit_mqtt_broker = QtWidgets.QLineEdit("localhost")
        self.edit_mqtt_broker.textChanged.connect(self._on_mqtt_edit_change)
        self.edit_mqtt_broker.returnPressed.connect(lambda: self._on_mqtt_set(False))
        l4.addRow("MQTT Broker", self.edit_mqtt_broker)

        l5 = QtWidgets.QHBoxLayout()
        l4.addRow("", l5)
        self.mqtt_set_button = QtWidgets.QPushButton("Set")
        self.mqtt_set_button.setEnabled(False)
        self.mqtt_set_button.clicked.connect(self._on_mqtt_set)
        l5.addWidget(self.mqtt_set_button)
        l5.addStretch()

        self._on_radio_select(False)

    def _on_radio_select(self, checked):
        rest = self.radio_REST.isChecked()

        # Enable/Disable REST/MQTT LineEdits
        self.edit_rest_host.setEnabled(rest)
        self.edit_rest_port.setEnabled(rest)
        self.edit_mqtt_broker.setEnabled(not rest)
        self.mqtt_set_button.setEnabled(False)

        # Emit settingsChagned event
        self.settingsChanged.emit((self.get_rest_connection(), self.get_mqtt_connection()))

    def _on_rest_edit_change(self, text):
        """
        Event handler for the REST-related QLineEdits, to emit settingsChanged event, whenever the user changes the REST
        connection settings.

        :param text: Unused
        """
        self.settingsChanged.emit((self.get_rest_connection(), self.get_mqtt_connection()))

    def _on_mqtt_edit_change(self, text):
        """
        Event handler for the MQTT-related QLineEdit, to enable the mqtt_set_button, when the user edited the MQTT
        broker address.

        :param text: Unused
        """
        self.mqtt_set_button.setEnabled(True)

    def _on_mqtt_set(self, checked):
        """
        Event handler for the MQTT set button QLineEdit, to emit settingsChanged event, when the users submits the new
        MQTT broker.

        :param checked: Unused
        """
        self.mqtt_set_button.setEnabled(False)
        self.settingsChanged.emit((self.get_rest_connection(), self.get_mqtt_connection()))

    def get_mqtt_connection(self):
        """
        Get the current MQTT connection settings, entered by the user.

        :return: Tuple, containing the following values: MQTT active, MQTT broker address
        :rtype: (bool, str)
        """
        return self.radio_MQTT.isChecked(), self.edit_mqtt_broker.text()

    def get_rest_connection(self):
        """
        Get the current REST connection settings, entered by the user.

        :return: Tuple, containing the following values: REST active, REST host name, REST port
        :rtype: (bool, str, int)
        """
        try:
            rest_port = int(self.edit_rest_port.text())
        except ValueError:
            rest_port = 0
        return self.radio_REST.isChecked(), self.edit_rest_host.text(), rest_port


class MachineStateWidget(QtWidgets.QGroupBox):
    def __init__(self, client, parent=None):
        super().__init__("MachineState", parent)
        self.client = client
        self._init_gui()

    def _init_gui(self):
        l = QtWidgets.QFormLayout()
        self.setLayout(l)

        self.combo_state = QtWidgets.QComboBox()
        l.addRow("State", self.combo_state)
        for state in MachineStates:
            self.combo_state.addItem(state.name, state.value)
        self.combo_state.currentIndexChanged.connect(self._on_state_change)

        self.slider_progress = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.slider_progress.setMaximum(100)
        self.slider_progress.setValue(100)
        self.slider_progress.valueChanged.connect(self._on_slider_change)
        l.addRow("Progress", self.slider_progress)

        self.slider_position = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.slider_position.valueChanged.connect(self._on_slider_change)
        l.addRow("Error Position", self.slider_position)

        self._update_enable_states()

    def _on_state_change(self, index):
        self._update_enable_states()
        self._send_current_state()

    def _on_slider_change(self, value):
        self._send_current_state()

    def _update_enable_states(self):
        state = MachineStates(self.combo_state.itemData(self.combo_state.currentIndex()))
        has_progress = state not in (MachineStates.OFF, MachineStates.READY)
        has_position = state in (MachineStates.WARNING, MachineStates.ERROR, MachineStates.CRITICAL,
                                 MachineStates.EXPLODING)
        self.slider_progress.setEnabled(has_progress)
        self.slider_position.setEnabled(has_position)

    def _send_current_state(self):
        state = MachineStates(self.combo_state.itemData(self.combo_state.currentIndex()))
        progress = self.slider_progress.value() / 100
        position = self.slider_position.value() / 100
        self.client.send_machine_state(state.value, progress, position)


class TerminalStateWidget(QtWidgets.QGroupBox):
    def __init__(self, client, parent=None):
        super().__init__("TerminalState", parent)
        self.client = client
        self._init_gui()

    def _init_gui(self):
        l1 = QtWidgets.QVBoxLayout()
        self.setLayout(l1)
        l2 = QtWidgets.QFormLayout()
        l1.addLayout(l2)

        self.combo_state = QtWidgets.QComboBox()
        l2.addRow("State", self.combo_state)
        for state in TerminalState:
            self.combo_state.addItem(state.name, state.value)
        self.combo_state.currentIndexChanged.connect(self._on_state_change)

        self.slider_progress = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.slider_progress.valueChanged.connect(self._on_slider_change)
        l2.addRow("Progress", self.slider_progress)

        self.button_touch = QtWidgets.QPushButton("Touch")
        self.button_touch.clicked.connect(self._on_touch_button)
        l1.addWidget(self.button_touch)

        self.slider_graph = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.slider_graph.valueChanged.connect(self._on_slider_graph_change)
        l1.addWidget(self.slider_graph)

        self._update_enable_states()

    def _on_state_change(self, index):
        self._update_enable_states()
        self._send_current_state()

    def _on_slider_change(self, value):
        self._send_current_state()

    def _on_touch_button(self, checked):
        self.client.trigger_terminal_feedback(1)

    def _on_slider_graph_change(self, value):
        self.client.trigger_terminal_bargraph(value / 100)

    def _update_enable_states(self):
        state = TerminalState(self.combo_state.itemData(self.combo_state.currentIndex()))
        has_progress = state in (TerminalState.BUSY, TerminalState.BUSY_BACKGROUND)
        self.slider_progress.setEnabled(has_progress)
        if not has_progress:
            self.slider_progress.setValue(0)

    def _send_current_state(self):
        state = TerminalState(self.combo_state.itemData(self.combo_state.currentIndex()))
        progress = self.slider_progress.value() / 100
        self.client.send_terminal_state(state.value, progress)


class ScriptPlayerWidget(QtWidgets.QGroupBox):
    def __init__(self, player, parent=None):
        super().__init__("Script Player", parent)
        self.player = player

        self._init_gui()

        player.progress_changed.connect(self._on_progress_change)
        self._on_script_change(0)

    def _init_gui(self):
        l = QtWidgets.QHBoxLayout()
        self.setLayout(l)

        self.combo_script = QtWidgets.QComboBox()
        for name, script in scripts.SCRIPTS:
            self.combo_script.addItem(name, script)
        l.addWidget(self.combo_script, 2)

        self.button_play = QtWidgets.QPushButton("Play")
        l.addWidget(self.button_play, 1)

        self.progress_label = QtWidgets.QLabel("Idle")
        self.progress_label.setAlignment(QtCore.Qt.AlignCenter)
        l.addWidget(self.progress_label, 1)

        self.combo_script.currentIndexChanged.connect(self._on_script_change)
        self.button_play.clicked.connect(self._on_play_button)

    def _on_script_change(self, index):
        self.player.set_script(self.combo_script.currentData())

    def _on_play_button(self, checked):
        if self.player.get_progress()[0]:
            self.player.stop()
        else:
            self.player.start()

    def _on_progress_change(self, progress):
        started, step, num_steps, step_time, total_time = progress

        self.button_play.setText("Play" if not started else "Stop")
        self.combo_script.setEnabled(not started)
        self.progress_label.setText("{} {}/{} ({:.0f}%)"
                                    .format("▶️" if started else "⏸",
                                            step, num_steps,
                                            step_time/total_time*100 if total_time != 0 else 0))
